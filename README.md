# De quoi s'agit-il ?

- FOAD bloc 5 pour le DIU-EIL de Lyon 2019/2020 
- Quatuor constitué de Brigitte Mougeot, Véronique Reynaud, Louise Martens et Frédéric Junier 
- Thème : Résolution de Sudoku par coloriage
- Licence : Creative Commons Attribution Share Alike 4.0   <https://gitlab.com/frederic-junier/foad-bloc5-mougeot-reynaud-martens-junier/-/blob/master/LICENSE>
- Gitlab : <https://gitlab.com/frederic-junier/foad-bloc5-mougeot-reynaud-martens-junier/>
- Video de présentation : <https://tube.ac-lyon.fr/videos/watch/87df9b56-cf16-404f-9d0d-c8495628b7b8>

# Présentation succincte:

Projet guidé de résolution de grilles de sudoku modélisées par un graphe non orienté. Les cases du sudoku sont les sommets du graphe et les contraintes pour remplir les cases sont les arêtes du graphe. Les élèves s’approprient le problème puis écrivent des fonctions pour générer le graphe des contraintes.
Des heuristiques gloutonnes et backtracks s’appuyant sur la coloration de graphes sont implémentées.
On compare leur efficacité versus leurs temps d’exécution. 
Une interface Tkinter termine le projet.


## Liste des fichiers fournis :

* __Le fichier de synthèse du projet [Sudoku.pdf](Sudoku.pdf)__
* Les scripts Python du projet complet : 
    *  sudoku.py
    *  sudoku_versionTKinter.py
* Les documents élèves dans l’ordre de leur utilisation : 
    - graphe-sudoku.ggb 	
    - Etape2.py	
    - Etape2_TODO.py 	
    - sudoku_heuristique_gloutonne_eleve.py
    - sudoku_heuristique_backtrack_eleve.py et cadeau.py et un dossier build avec un binaire exécutable de sudoku_versionTKinter.py (pour masquer le code source)
    - Etape5_TODO1.py et Etape5_TODO1_corrige.py	
    - Etape5_TODO2.py
* Dépôt en ligne : <https://gitlab.com/frederic-junier/foad-bloc5-mougeot-reynaud-martens-junier/>


## Etape 1 : Le Graphe Sudoku
- Appropriation du problème de résolution de sudoku papier crayon
- Modélisation sous forme d’un graphe non orienté papier crayon
- Utilisation de GeoGebra


## Etape 2 : Vers une classe Sudoku 
- Préliminaires papier crayon
- Scripts de diverses fonctions
- Génération du graphe des contraintes
- Fonction de vérification d’une grille de sudoku
- Promotion en classe

## Etape 3 : Coloriage de Sudoku - heuristiques gloutonnes 
- Le défaut d’une approche force-brute
- 1ère heuristique : sommets traités dans l’ordre, couleur choisie aléatoirement
- 2ème heuristique : sommets qui ont le plus de contraintes traités en premier et ses variantes

## Etape 4 : Coloriage de Sudoku - heuristiques brute force avec backtracking
- Implémentation d’une heuristique brute force avec backtrack
- Implémentation d'une variante de l'heuristique brute force avec backtrack

## Etape 5 : Euler, fichiers textes, décorateurs, matplotlib 

## Etape 6 : Interface Tkinter 
