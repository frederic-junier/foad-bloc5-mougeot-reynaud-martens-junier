def test_euler(heuristique, fichierSudokus = 'p096_sudoku.txt'):
    """Test d'une heuristique sur le banc d'essai des 50 grilles du projet Euler 96"""
    print()
    print("Test de l'heuristique ",heuristique.__name__,"sur le fichier de grilles du problème 96 du projet Euler ",fichierSudokus)
    les_temps = []       
    les_essais = []
    les_succes = []
    #initialiser une variable fichier qui ouvre le fichier fichierSudokus en lecture
    #TODO
    i = 0
    while  fichier.readline():
        #initialiser une liste vide GrilleDefiEuler qui contiendra la grille de Sudoku
        #TODO 
        #créer une boucle qui extrait chaque ligne de la grille et l'ajoute à la variable GrilleDefiEuler
        #TODO
        
        
        
        print("Grille n°",i+1)   #n'affiche pas la grille mais montre l'évolution...
        sudo = Sudoku(3)
        sudo.generer_graphe()
        rep, temps = heuristique(sudo, GrilleDefiEuler)
        nb_essais = 1
        if isinstance(rep, list):
            solution = rep
        else:
            nb_essais, solution = rep
        #ajouter qqc dans une liste
        if solution is not None and sudo.verification_grille(solution):
            #ajouter qqc dans une liste
        else:
            #ajouter qqc dans une liste
        #ajouter qqc dans une liste
        i += 1
    fichier.close()
    return (les_temps, les_succes, les_essais)

def graphique_resolues_euler(heuristique, itermax, version):
    """Génère un graphique spécifique pour l'heuristique de coloration"""

    @wraps(heuristique)
    def wraped_heuristique(*args, **kwargs):
        return heuristique(*args, **kwargs, itermax = itermax, version = version)

    _, les_succes, les_essais = test_euler(wraped_heuristique)
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.scatter(np.arange(1, 51), les_succes, label = 'grilles résolues', marker='x', color ='red')
    ax2.semilogy(np.arange(1, 51), les_essais, label = "nombre d'essais")
    ax1.set_ylabel('réussies (1 ou 0)')
    ax2.set_ylabel("nombre d'essais")
    fig.suptitle(f"{sum(les_succes)} grilles résolues sur 50 en {itermax} essais max")
    fig.legend(loc='center right')
    fig.savefig(f"grilles_resolues_pb96_ProjetEuler_heuristique_{wraped_heuristique.__name__}_itermax_{itermax}_version_{version}.pdf")
    
def graphique_comparaison_euler(*liste_heuristique, itermax= 1000, versionglouton = 'hasard'):  
    """Génère des graphiques des séries chronologiques des temps d'exécution de plusieurs heuristiques
    sur le banc d'essai des 50 grilles du projet Euler 96"""
    fig, axs = plt.subplots(len(liste_heuristique) + 2, 1)
    marqueurs = Line2D.filled_markers
    fig.set_size_inches((20, 30))  
    FONT_SIZE = 20
    for k, heuristique in enumerate(liste_heuristique):
        
        @wraps(heuristique)
        def wraped_heuristique(*args, **kwargs):
            return heuristique(*args, **kwargs, itermax = itermax, version = versionglouton)
        les_temps_methode, les_succes, __ = test_euler(wraped_heuristique)
        axs[k].scatter(np.arange(0, 50), les_succes,marker=marqueurs[k % len(marqueurs)])
        axs[k].set_yticks([0,1])
        axs[k].set_yticklabels(["Non résolue", "Résolue"], fontsize = FONT_SIZE)
        axs[k].annotate(f"{heuristique.__name__} : \n  {sum(les_succes)} / 50 grilles résolues",(10,0.3), fontsize = FONT_SIZE)
        #axs[k].legend()
        axs[-2].semilogy(np.arange(0, 50), les_temps_methode)
        axs[-1].plot(np.arange(0, 50), les_temps_methode, label=wraped_heuristique.__name__)
      

    axs[-2].set_ylabel('Temps  log', fontsize = FONT_SIZE)   
    axs[-1].set_ylabel('Temps linéaire', fontsize = FONT_SIZE)    
    axs[-1].set_xlabel('Grille', fontsize = FONT_SIZE)
    fig.suptitle(f"Banc d'essai des 50 grilles du projet Euler 96, \n itermax glouton = {itermax} ", fontsize = FONT_SIZE)
    fig.legend(loc='lower center', fontsize = FONT_SIZE)
    #plt.subplots_adjust(hspace=1)
    fig.savefig(f'Banc-essai-euler-itermax-glouton-{itermax}-version-glouton-{versionglouton}.pdf')

    
