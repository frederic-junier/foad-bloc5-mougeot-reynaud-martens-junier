import time
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from functools import wraps
import random

##################################################################


# EXERCICE 1 
#créer un décorateur qui permet de vérifier si l'utilisateur à le droit d'utiliser la fonction

user = "Jacques"
def verificateur_utilisateur(func):
    def wrapper(*args, **kwargs):
        if user =="Jacques":
            response = func(*args, **kwargs)
            return response
        else:
            print("l'utilisateur n'a pas les droits d'acces")
	    return None
        
    return wrapper

#verifier si votre décorateur fonctionne en changeant le nom de l'utilisateur
@verificateur_utilisateur
def modification_fichier():
    print("Quels sont les modifications?")
    return None

modification_fichier()

# EXERCICE 2
#créer un décorateur chronomètre qui permet de caluler le temps d'une fonction
#appliquer le décorateur suivant à la fonction fibonnaci

def chronometre(f):
    """Retourne le temps d'exécution et le résultat
                    
    d'une fonction f"""
    @wraps(f)
    def wraped(*args, **kwargs):
        """Fonction enveloppée"""
        debut = time.perf_counter()
        rep = f(*args, **kwargs)
        temps = time.perf_counter() - debut
        return (rep, temps)
    
    return wraped

@chronometre
def factorielle(n):
    fact = 1
    i=1
    while i <=n:
        fact = fact*i
        i=i+1
    return fact

    
print(factorielle(105))




