# EXEMPLE

def affichage_decorateur(function):
    def ma_fonction_a_decorer(*args, **kwargs):
        print('Appel de la fonction {} avec args={} et kwargs={}'.format(
            function.__name__, args, kwargs))
        ret = function(*args, **kwargs)
        print('Retour: {}'.format(ret))
        return ret
    
    return ma_fonction_a_decorer

@affichage_decorateur 
def addition(a, b):
     return a + b

addition(3,2)

##################################################################

# EXERCICE 1 
#créer un décorateur qui permet de vérifier si l'utilisateur à le droit d'utiliser la fonction

user = "Jacques"
def verificateur_utilisateur(func):
    def wrapper(*args, **kwargs):
        # TODO verifier si l'utilisateur est valide (Jacques ok, autres utilisateur non
        #si oui on peut executer la fonction,
        #sinon afficher un message d'erreur 
        response = func(*args, **kwargs)
        return response
    
    return wrapper

#verifier si votre décorateur fonctionne en changeant le nom de l'utilisateur
@verificateur_utilisateur
def modification_fichier():
    print("Quels sont les modifications?")
    return

# EXERCICE 2
#créer un décorateur chronomètre qui permet de caluler le temps d'une fonction
#appliquer le décorateur suivant à la fonction factorielle

def chronometre(f):
    """Retourne le temps d'exécution et le résultat d'une fonction f"""
    @wraps(f)
    def wraped(*args, **kwargs):
        """Fonction enveloppée"""
        #TODO completer les instruction à faire avant d'executer la fonction
        #création d'une nouvelle variable temps
        #temps = ???
        rep = f(*args, **kwargs)
        #TODO completer les instruction à faire après l'execution de la fonction
        return (rep, temps)
    
    return wraped


def factorielle(n):
    fact = 1
    i=1
    while i <=n:
        fact = fact*i
        i=i+1
    return fact
    
print(factorielle(105))




