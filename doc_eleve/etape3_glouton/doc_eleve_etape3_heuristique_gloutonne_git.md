# Préambule

Le problème de résolution d’un Sudoku à
![n](https://latex.codecogs.com/png.latex?n "n") chiffres dans une
grille ![n \\times
n](https://latex.codecogs.com/png.latex?n%20%5Ctimes%20n "n \\times n")
(avec ![n](https://latex.codecogs.com/png.latex?n "n") carré d’entier)
est un problème difficile pour lequel il n’existe pas d’algorithme de
résolution connu de complexité temporelle polynomiale, c’est-à-dire
avec un nombre d’opérations élémentaires inférieur ou égal à ![C
n^{k}](https://latex.codecogs.com/png.latex?C%20n%5E%7Bk%7D "C n^{k}")
avec ![C](https://latex.codecogs.com/png.latex?C "C") une constante et
![k](https://latex.codecogs.com/png.latex?k "k") un entier naturel.

En revanche, on a vu qu’il est possible de vérifier si une grille de
Sudoku est correctement remplie avec une complexité temporelle de
l’ordre de la taille de la grille à une constante près.

D’autres questions intéressantes sur les Sudokus sont par exemple le
nombre minimum de données pour qu’une grille ait une unique solution :
en 2011, des chercheurs ont démontré que ce nombre minimum
d’informations nécessaires est de 17 pour un Sudoku à 9 chiffres. On
pourra lire sur ce sujet cet artile
<https://www.pourlascience.fr/sd/mathematiques/le-probleme-du-sudoku-8241.php>.

Pusique qu’on ne dispose pas d’algorithme de complexité raisonnable pour
résoudre une grille quelconque de Sudoku, on va utiliser des
**heuristiques** qui sont des méthodes de calcul permettant d’atteindre
rapidement une solution et d’échapper à l’explosion combinatoire de
l’étude exhaustive e toutes les possibilités.

1.  Supposons qu’on essaie de résoudre une grille de Sudoku avec un
    algorithme brute force qui teste toutes les grilles possibles sans
    examiner les contraintes.

<!-- end list -->

  - Déterminer un ordre de grandeur du nombre de possibilités pour une
    grille de Sudoku ![9
    \\times 9](https://latex.codecogs.com/png.latex?9%20%5Ctimes%209
    "9 \\times 9") dont ![36](https://latex.codecogs.com/png.latex?36
    "36") cases sont initialement fixées.

  - Donner un ordre de grandeur du nombre de jours qu’il faudrait à une
    machine, capable de tester capable de tester
    ![10^{6}](https://latex.codecogs.com/png.latex?10%5E%7B6%7D
    "10^{6}") possibilités par seconde, pour explorer toutes ces
    possibilités ?

Puisqu’un tel algorithme n’est pas raisonnable, on va utiliser des
heuristiques qui sont des méthodes de calcul permettant d’atteindre
rapidement une solution et d’échapper à l’explosion combinatoire de
l’étude exhaustive de toutes les possibilités.

# Une première heuristique de coloriage gloutonne

Dans l’étape 1, nous avons montré que la résolution d’une grille de
Sudoku est équivalente à un problème de coloration de graphe où chaque
case/sommet est reliée par une arête aux cases/sommets de la même ligne,
de la même colonne ou du même bloc.

La coloration de graphe consiste à attribuer une couleur à chacun de ses
sommets de manière que 2 sommets reliés par une arête soient de couleur
différente. On cherche souvent à utiliser le moins de couleurs
possibles.

Dans la suite, on travaille sur une instance de la classe Sudoku
construite à la fin de l’étape 2, nommée `sudo`. Cette classe contient
une méthode generer\_graphe qui initialise l’attribut `adj`. Ainsi
`sudo.adj` est une liste de listes qui permet d’obtenir, pour chaque
case/sommet, la liste de ses voisins c’est-à-dire la liste des
cases/sommets dans une même ligne, une même colonne ou un même bloc.

Voici une description d’une première heuristique gloutonne de coloriage
en pseudo-code :

  - **Étape 1 :**
      - Avec la méthode conversion\_grille\_vers\_couleur, on initialise
        une liste couleur, de taille le nombre total de cases, avec les
        couleurs déjà fixées (de 1 à nbCouleur) et 0 sinon ;
      - On initialise une variable nombreAcolorier avec le nombre de
        cases/sommets non coloriés dans la liste couleur (soit le nombre
        de cases telles que couleur\[case\] == 0) ;
      - On initialise case à 0.
  - **Étape 2 :**
      - À partir de la case courante, si elle n’a pas de couleur
        (couleur\[case\] == 0), on initialise une variable
        couleurs\_possibles avec la liste de toutes les couleurs
        possibles ;
      - On fait une boucle sur les voisins grâce à l’attribut adj et
        pour chaque voisin on enlève la couleur du voisin si elle fait
        partie de la liste couleurs\_possibles ;
      - Ensuite si couleurs\_possibles est non vide, on fixe la couleur
        de case en choisissant aléatoirement une couleur parmi
        couleurs\_possibles et on décrémente nombreAcolorier de 1 ;
      - On incrémente case de 1 ;
      - Si toutes les cases de la grille n’ont pas été visitées (case \<
        nbcases) et si toutes les cases ne sont pas encore coloriées
        (nombreAcolorier \> 0), on reprend à l’étape 2, sinon on passe à
        l’étape 3.
  - **Étape 3 :**
      - Si nombreAcolorier est égale à 0, on a trouvé une solution : on
        a colorié toutes les cases/sommets, la grille est remplie et on
        a terminé.

<!-- end list -->

1.  Que pensez-vous à priori de cet algorithme ?
2.  Pour quels types de Sudoku pensez-vous qu’il pourrait être
    efficace ?
3.  On a qualifié cette heuristique de “gloutonne”. Quels choix sont
    effectués lors de l’étape 2 ?

## Répétition de l’heuristique de coloriage gloutonne

Une idée est de recommencer toutes les étapes depuis le début si on n’a
pas colorié toutes les cases : comme le choix de la couleur à l’étape 2
est aléatoire, chaque exécution de l’heuristique peut être différente et
on peut espérer trouver une solution “par hasard”. On parle dans ce cas
d’**algorithme randomisé**. On peut ainsi fixer un nombre maximum
`itermax` d’itérations de l’heuristique et retourner la grille solution
dès qu’on l’a trouvée (sortie prématurée) avec la méthode
`conversion_couleur_vers_grille` ou `None` si on a épuisé toutes les
tentatives sans résoudre le problème.

1.  Dans le fichier fourni `sudoku_heuristique_gloutonne_eleve.py`,
    implémentez cette première heuristique gloutonne à travers la
    fonction `resolution_sudoku_coloration_glouton`, qui est une méthode
    membre de la classe `Sudoku`. . Les parties à compléter sont
    signalées par des commentaires `#### TO DO à compléter` et on donne
    ci-dessous un diagramme d’exécution (flowchart réalisé avec
    [draw.io](https://app.diagrams.net/)).
2.  Exécutez le script `sudoku_heuristique_gloutonne_eleve.py` et
    comparez les résultats obtenus par les fonctions
    `resolution_sudoku_coloration_glouton` et
    `resolution_sudoku_coloration_glouton_tri1`.
3.  On propose d’écrire une fonction
    `resolution_sudoku_coloration_glouton_tri` similaire à la
    précédente, mais avec deux variantes supplémentaires au niveau du
    choix de la prochaine case à colorier :

<!-- end list -->

  - en sélectionnant parmi les cases qui ont le plus de contraintes,
    c’est-à-dire dans la liste liste\_candidat, celles qui ont le
    moins de voisins non coloriés ;
  - en sélectionnant parmi les cases qui ont le plus de contraintes,
    c’est-à-dire dans la liste liste\_candidat, celles qui ont le plus
    de voisins non coloriés. Le paramètre version de la fonction
    `resolution_sudoku_coloration_glouton_tri` permettra de sélectionner
    l’une des trois variantes de l’heuristique. Implémentez cette
    fonction en complétant les parties signalées par des commentaires
    `#### TO DO` à compléter dans le fichier
    `sudoku_heuristique_gloutonne_eleve.py`. Cette méthode membre de la
    classe `Sudoku` contient trois fonctions outils plus\_petit,
    plus\_grand et `selection_extremum_voisins` qu’il faut compléter en
    respectant la spécification donnée dans la documentation.

![diagramme heuristique gloutonne 1](images/ResolutionGlouton.png)  

# Amélioration de notre heuristique de coloriage gloutonne

Dans l’heuristique précédente le choix glouton était fixé à l’avance par
l’ordre de parcours des cases/sommets et on ne cherchait pas à optimiser
une certaine quantité. Il peut sembler pertinent de sélectionner dans
une liste `liste_candidat` les prochaines cases à colorier parmi celles
dont le nombre de couleurs possibles est minimal. En choisissant
aléatoirement la case colorier parmi celles de `liste_candidat`, on
obtient une heuristique randomisée de coloriage que nous pourrons itérer
comme la précédente jusqu’au coloriage de toutes les cases (sortie
prématurée) ou jusqu’à l’épuisement du nombre maximal `itermax`
d’itérations.

1.  Dans le fichier fourni `sudoku_heuristique_gloutonne_eleve.py`,
    implémentez cette première heuristique gloutonne à travers la
    fonction `resolution_sudoku_coloration_glouton_tri1` qui est une
    méthode membre de la classe `Sudoku`. Les parties à compléter sont
    signalées par des commentaires `#### TO DO à compléter` et on donne
    ci-dessous un diagramme d’exécution (flowchart).
2.  Exécutez le fichier `sudoku_heuristique_gloutonne_eleve.py` et
    comparez les résultats obtenus par les fonctions
    `resolution_sudoku_coloration_glouton` et
    `resolution_sudoku_coloration_glouton_tri1`.
3.  On propose d’écrire une fonction
    `resolution_sudoku_coloration_glouton_tri` similaire à la
    précédente, mais avec deux variantes supplémentaires au niveau du
    choix aléatoire de la prochaines case à colorier si `nbcouleur_min
    != 1`:
      - en sélectionnant parmi les cases/sommets dans `liste_candidat`
        celles qui ont un nombre minimum de voisins non coloriés
      - ou en sélectionnant parmi les cases/sommets dans
        `liste_candidat` celles qui ont un nombre maximum de voisins non
        coloriés Le paramètre `version` de la fonction
        `resolution_sudoku_coloration_glouton_tri` permettra de
        sélectionner l’une des trois variantes de l’heuristique.

Implémentez cette fonction en complétant les parties signalées par des
commentaires `#### TO DO à compléter` dans le fichier
`sudoku_heuristique_gloutonne_eleve.py`. Cette méthode membre de la
classe `Sudoku` contient deux fonctions outils `plus_grand` et
`selection_extremum_voisins` qu’il faut commencer compléter en
respectant la spécification donnée dans la documentation.

4.  Exécutez le fichier `sudoku_heuristique_gloutonne_eleve.py`, tous
    les tests contenus dans les fonctions `test_coloration` et
    `test_diabolique` devraient être réalisées. Comparez et commentez
    les résultats obtenus par les différentes heuristiques.

![diagramme heuristique gloutonne
tri](images/ResolutionGloutonTriV1.png)
