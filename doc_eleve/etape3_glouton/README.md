Ce répertoire contient l'énoncé d'une activité de présentation de deux heuristiques de résolution de Sudoku 
par coloriage glouton.

Le `Makefile` permet de générer l'énoncé à partir de la source  `doc_eleve_etape3_heuristique_gloutonne.md` et des images dans le répertoire `images`.

Le répertoire `etape3_glouton_version-eleve` contient le script de travail des élèves et le corrigé.

La version Markdown adaptée pour un affichage dans Gitlab est `doc_eleve_etape3_heuristique_gloutonne_git.md`.
