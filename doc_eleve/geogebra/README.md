Ce répertoire contient une application Geogebra permettant de colorier le graphe associé à une grille de Sudoku 4 x 4, qui est aussi accessible en ligne <https://www.geogebra.org/m/jbksbgqe>. 

L'interaction entre l'utilisateur et la figure est gérée par un code Javascript accessible depuis les propriétés de n'importe quel objet dans l'onglet Script/Javascript global.

La documentation de l'API Javascript de Geogebra est accessible ici : <https://wiki.geogebra.org/fr/R%C3%A9f%C3%A9rence:GeoGebra_Applis_API>.

Voici le code :

```javascript
//tableau des 5 couleurs
var colour = [[255,255,255],[255,0,0], [0,255,0], [0,0,255],[255,255,0]];
//dictionnaire compteur de clic
var clicCounter = {
};
//tableau avec les noms des objets
var objets = ['C' ,'B' ,'D' ,'E','F','G','H', 'I','J','K','L','M','N','O','P','Q'];
//dictionnaire contenant les listes d'adjacence
var voisins = {
'D' : ['C', 'B', 'Q', 'P', 'L', 'H', 'O'],
'C' : ['D', 'B','Q', 'O', 'K','G','P'],
'B':['C','Q','D','N', 'J','F','M'],
'Q':['D','C','B','M', 'I', 'E'],
'P' : ['O', 'N', 'M','D', 'L', 'H', 'C'],
'O':['C','K','G','P', 'N', 'M','D'],
'N':['P','O','M','B','J','F','Q'],
'M':['P','O','N','Q', 'I', 'E','B'],
'L':['K','J','I','D', 'P', 'H','G'],
'K':['L','J','I','C','O','G','H'],
'J':['L','K','I','B','N','F','E'],
'I':['L','K','J','Q','M','E','F'],
'H':['G','F','E','D','L','P','K'],
'G':['H','F','E','C', 'O','K','L'],
'F':['B','N','J','H','G','E','I'],
'E':['H','G','F','Q','M','I','J']
};

//fonction de test de compatibilité de coloriage
function compatibilite(nomObj){
     for (var i = 0; i < voisins[nomObj].length;i++){
         if (clicCounter[nomObj] !== 0
      && clicCounter[nomObj] === clicCounter[voisins[nomObj][i]]){
     return false;
}
     }
return true;
}


//Gestionnaire de click sur un objet
function click(nomObj){
if (nomObj==='R'){
   for (var k = 0; k < objets.length;k++){
     ggbApplet.setColor(objets[k], 255,255,255); 
      clicCounter[objets[k]] = 0;
}
}
else{
clicCounter[nomObj] = (clicCounter[nomObj] + 1) % 5;

var k = clicCounter[nomObj];

if ( compatibilite(nomObj)){
ggbApplet.setColor(nomObj, colour[k][0],
	 colour[k][1],
	colour[k][2]);
}
else {
 alert("incompatibilité");
}
}
}


//Fonction d'initialisation appelée au démarrage de la construction
function ggbOnInit(){  
  //on attache le gestionnaire d'événement click	
  ggbApplet.registerClickListener("click");
  //on colorie tous les sommets en blanc
  for (var k = 0; k < objets.length;k++){
     ggbApplet.setColor(objets[k], 255,255,255); 
      clicCounter[objets[k]] = 0;
}
}
```