Ce répertoire contient :

* Le fichier de synthèse de notre projet aux formats `pdf` :  `Sudoku.pdf` et `docx` : `Sudoku.docx`. Il contient les énoncés de toutes les activités.
* Un sous-répertoire `etape2` avec les scripts Python (élève + corrigé) pour l'étape 2 de notre projet : fonctions nécessaires à la génération du graphe d'incompatibilité de couleur entre les cases/sommets, de vérification de grille etc ...
* Un sous-répertoire `etape3` avec le matériel (énoncé + script) pour l'étape 3 de notre projet : présentation d'heuristiques gloutonnes de résolution de grille de Sudoku par coloriage.
* Un sous-répertoire `etape4` avec le matériel (énoncé + script) pour l'étape 4 de notre projet : présentation d'heuristiques de résolution de grille de Sudoku par coloriage par exploration exhaustive (brute force) combinée avec un retour sur trace (backtracking).
* Un sous-répertoire `etape5` avec le matériel (scripts élèves) pour l'étape 5 de notre projet : comparaison des différentes heuristiques au banc d'essai des 50 grilles du [projet Euler 96](https://projecteuler.net/problem=96).
* Un sous-répertoire `etape6` avec le matériel (scripts de correction) pour l'étape 6 de notre projet : réalisation d'une interface graphique avec Tkinter pour la résolution par coloriage  d'une grille de Sudoku 9 x 9.
* Un sous-répertoire `geogebra` qui contient une application Geogebra permettant de colorier le graphe associé à une grille de Sudoku 4 x 4, qui est aussi accessible en ligne <https://www.geogebra.org/m/jbksbgqe>. 
