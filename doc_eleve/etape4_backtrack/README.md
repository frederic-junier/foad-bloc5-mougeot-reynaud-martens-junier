Ce répertoire contient l'énoncé d'une activité de présentation de deux heuristiques de résolution de Sudoku 
par coloriage avec exploration exhaustive et retour sur trace (backtracking).

Le `Makefile` permet de générer l'énoncé à partir de la source  `doc_eleve_etape3_heuristique_backtrack.md` et des images dans le répertoire `images`.

Le répertoire `etape4_gbacktrack_version-eleve` contient le script de travail des élèves et le corrigé.

Le répertoire `build` contient les scripts permettant de générer un exécutable pour un script de démonstration de résolution de Sudoku par backtracking dans une interface graphique en mode pas à pas.

L'intérêt de l'exécutable est de masquer le code des fonctions de résolution que les élèves doivent coder. 

L'archive `materiel.zip` contient tous les fichiers nécessaires au travail des élèves.

La version Markdown adaptée pour un affichage dans Gitlab est `doc_eleve_etape3_heuristique_backtrack_git.md`.
