Déballer l’archive `materiel.zip`, qui contient deux scripts Python et
un dossier `build`.

![arborescence](images/arborescence.png)  

L’idée est de partir d’un sommet du graphe et de faire une sorte de
parcours en profondeur. On descend dans le graphe, en coloriant les
sommets en choisissant une couleur selon les contraintes, et lorsque
qu’un sommet ne peut être colorié, on remonte en testant une autre
couleur parmi les possibles. Toutes les possibilités seront alors
explorées, sauf, si l’on tombe sur une solution.

# Résolution brute force d’une grille de Sudoku ![4 \\times 4](https://latex.codecogs.com/png.latex?4%20%5Ctimes%204 "4 \\times 4").

![grille 1](images/sudoku_4x4.png)  

1.  Résoudre la grille de Sudoku ![4
    \\times 4](https://latex.codecogs.com/png.latex?4%20%5Ctimes%204
    "4 \\times 4") ci-dessus en appliquant l’algorithme dont on donne le
    pseudo-code ci-dessous :
    
      - **Initialisation :** On part de la première case en haut à
        gauche et on commence l’étape **Exploration**
      - **Exploration :**
          - Si la couleur de la case est déjà fixée, on explore la case
            suivante, sachant que l’on parcourt les cases de haut en bas
            et de gauche à droite.
          - Sinon, on liste les couleurs possibles par rapport aux
            couleurs déjà fixées des “voisins” : les cases dans la même
            ligne, la même colonne ou le même bloc.
              - S’il reste au moins une couleur possible, on choisit
                celle de plus petit numéro et on explore la case
                suivante.
              - Sinon, on revient sur ses traces (backtracking en
                anglais) à la case précédente et on teste une autre
                couleur parmi les possibles en reprenant l’étape
                **Exploration**, s’il n’y en a pas, on revient encore
                sur ses traces…
      - **Fin :** Il ne reste plus de case à explorer, la grille est
        résolue

![grille 1](images/sudoku_4x4_bis.png)  

2.  Pourquoi peut-on qualifier cet algorithme de *brute force* avec
    *retour sur trace* ?
3.  Que faut-il prévoir pour reprendre l’exploration lorsqu’on effectue
    un *retour sur trace* ?
4.  Si une grille de Sudoku possède une solution, cet algorithme peut-il
    la trouver de façon certaine ?
5.  Quel est le défaut de cet algorithme ?
6.  Ouvrir le dossier `build` puis le sous-dossier correspondant au
    système d’exploitation de sa machine. Par exemple
    `exe.win-amd64-3.7` pour une machine 64 bits sous Windows 10, puis
    exécuter le fichier binaire
    `sudoku_versionTKinterDemoBacktracking.exe` (sans extension sou
    Linux). Sélectionnez le mode `bactracking pas à pas` dans la fenêtre
    pop-up puis visualiser le début de l’exécution de la résolution
    d’une grille de Sudoku ![9
    \\times 9](https://latex.codecogs.com/png.latex?9%20%5Ctimes%209
    "9 \\times 9") par exploration exhaustive avec retour sur trace.
    Cette application a été programmée en Python avec le module Tkinter.
    Quelle est la différence entre le fichier binaire qui est exécuté et
    un script Python ?

**Remarque :** Cet exécutable a été généré en suivant ce tutoriel
<https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/235020-distribuer-facilement-nos-programmes-python-avec-cx-freeze>.
Le dossier `etape4_demo_backtracking` contient les scripts Python
nécessaires pour générer un exécutable sur n’importe quelle plateforme
en exécutant `python3 setup.py build` sous Linux ou `py setup.py build`
dans un environnement Anaconda sous Windows. Il faut d’abord installer
le module `cx-freeze` avec `pip`.

![tkinter](images/backtrack-demo2.png)  

7.  Quelle méthode de programmation découverte cette année permettrait
    d’implémenter le plus naturellement l’algorithme *exploration
    exhaustive* ? Cette méthode a été aussi utilisée dans l’étude des
    arbres binaires et l’exploration exhaustive peut justement être
    représentée par un arbre. Compléter en mettant les numéros des
    couleurs :

![arbre](images/arbre.png)  

Avec la résolution du Sudoku effectuée dans la question 1, entourer dans
l’arbre ci-dessus, la branche qui a mené à une impasse, celle qui a mené
à la solution et celle qui n’a donc pas été explorée.

# Implémentation de l’exploration exhaustive avec avec retour sur trace

Ouvrir le fichier `sudoku_heuristique_backtrack_eleve.py`. On
s’intéresse à la fonction `resolution_sudoku_backtrack`, méthode
membre de la classe `Sudoku`.

1.  Quelle est la valeur de la variable `couleur` ?

<!-- end list -->

``` python
def resolution_sudoku_backtrack(self, grille, *args, **kwargs) :
             
        couleur = self.conversion_grille_vers_couleur(grille)
  
        def exploration(case, couleur):  
            # plus de case à explorer, on retourne la grille solution
            if case == self.nbcases:
                solution = self.conversion_couleur_vers_grille(couleur)
                return (True, solution)
            #case déjà coloriée, on passe à la case suivante
            if couleur[case] > 0:
                return exploration(case + 1, couleur)
            else:          
                #ensemble des couleurs des voisins     
                couleurs_voisins = set()
                for voisin in self.adj[case] :
                    couleurs_voisins.add(couleur[voisin])
                #pour chaque couleur qui n'est pas dans couleurs_voisins
                #on la choisit pour couleur[case] et on explore la case suivante
                for c in range(1, self.nbcouleurs + 1) :
                    #### TO DO à compléter
                    "à compléter"
                #backtracking : le sous-arbre ne comporte pas de solution
                #on revient sur trace mais il faut penser à effacer ses traces !
                #### TO DO à compléter
                return (False, None)
                
        #appel de la fonction d'exploration
        _, solution =  exploration(0, couleur)
        return solution
```

2.  Quel type de valeurs est retourné par la fonction `exploration` ?
3.  Dans quel ordre sont explorées les cases ?
4.  Dans quel cas la fonction `resolution_sudoku_backtrack`
    retourne-t-elle une grille solution ?
5.  Dans quel cas la fonction `exploration` retourne-t-elle `(False,
    None)` ?
6.  Quelle action faut-il réaliser juste avant de revenir sur ses traces
    (backtracking) ?
7.  Que fait l’instruction `if couleur[case] > 0` ?
8.  On donne ci-dessous deux tests de performance réalisés avec le
    module timeit, pour des listes et des ensembles de même taille.

<!-- end list -->

``` python
>>> import timeit
>>> def temps_moyen(expression, nbrepeat):
...     return timeit.timeit(expression, number = nbrepeat) / nbrepeat
... 
>>> maliste = list(range(10**6))
>>> temps_moyen(lambda : 500000 in maliste, 1000)
0.006062848554000084
>>> monensemble = set(range(10**6))
>>> temps_moyen(lambda : 500000 in monensemble, 1000)
2.2288300169748253e-07
```

Dans quel type de variable sont stockées les couleurs des voisins ?
Justifier ce choix.

9.  Compléter le code de la fonction `resolution_sudoku_backtrack`,
    méthode membre de la classe `Sudoku`, au niveau des commentaires
    `### TO DO à compléter`.

## Variante

Copier, dans le fichier `cadeau.py` fourni, la fonction
`resolution_sudoku_backtrack_tri`, et la coller dans le fichier
`sudoku_heuristique_backtrack_eleve.py` comme méthode membre de la
classe Sudoku.

Exécuter le fichier `sudoku_heuristique_backtrack_eleve.py`. Trois
fonctions `test_coloration`, `test_backtrack` et `test_diabolique`
permettent de tester des heuristiques de résolution sur différentes
grilles de Sudoku 4 × 4 et 9 × 9. Comparer les performances des
différentes heuristiques et de leurs variantes.
