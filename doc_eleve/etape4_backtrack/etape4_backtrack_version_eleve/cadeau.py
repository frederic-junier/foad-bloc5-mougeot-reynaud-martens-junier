    @chronometre
    def resolution_sudoku_backtrack_tri(self, grille, *args, **kwargs) :
        '''Voir https://projecteuler.net/problem=96
           Voir http://igm.univ-mlv.fr/~dr/XPOSE2013/sudoku/backtracking.html#principe'''
        def couleurs_possibles(case, couleur):                
            crible = [True for _ in range(self.nbcouleurs)]
            couleurs_voisins = set()
            for voisin in self.adj[case] :
                couleurs_voisins.add(couleur[voisin])
            #print("C", couleurs_voisins)
            for c in couleurs_voisins :
                if c != 0: #attention aux index négatifs en python !
                    crible[c - 1] = False
            return [k + 1 for k, possible in enumerate(crible) if possible]

        couleur = self.conversion_grille_vers_couleur(grille)
        liste_cases_tri = sorted([case for case in range(self.nbcases) if couleur[case] == 0],
                                     key = lambda case : len(couleurs_possibles(case, couleur)))
        nb_cases_vides = len(liste_cases_tri)
        #liste_solutions = []

        def exploration(index_tri_case, couleur):  
            if index_tri_case == nb_cases_vides :
                return    (True, self.conversion_couleur_vers_grille(couleur))             
            case = liste_cases_tri[index_tri_case]      
            couleurs_voisins = set()
            for voisin in self.adj[case] :
                couleurs_voisins.add(couleur[voisin])        
            for c in range(1, self.nbcouleurs + 1) :
                if c not in couleurs_voisins:
                    couleur[case] = c
                    copie_couleur = couleur[:]
                    rep = exploration(index_tri_case  + 1, copie_couleur)
                    if rep[0]:
                        return rep
            #backtracking
            couleur[case] = 0  
            return (False, None)  
                    
        #appel de la fonction d'exploration
        _, solution =  exploration(0, couleur)
        return solution