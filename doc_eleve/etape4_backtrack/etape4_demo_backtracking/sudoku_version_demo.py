#!/usr/bin/env python
# -*- coding: utf-8 -*-

##################################################################
##                Importation des bibliothèques                 ##
##################################################################

import time
from functools import wraps
import random

##################################################################
##                       Fonction outils                        ##
##################################################################

def chronometre(f):
    """Retourne le temps d'exécution et le résultat
    d'une fonction f"""
    
    @wraps(f)
    def wraped(*args, **kwargs):
        """Fonction enveloppée"""
        debut = time.perf_counter()
        rep = f(*args, **kwargs)
        temps = time.perf_counter() - debut
        return (rep, temps)
    
    return wraped

##################################################################
##                        Classe Sudoku                         ##
##################################################################

class Sudoku() :

    def __init__(self, cote_case = 3) :                 #mettre 3 ou 2
        self.nbcases = cote_case ** 4                   #nombre de cases du sudoku 16 (de 0 à 15) ou 81 (de 0 à 80)
        self.nbcouleurs = cote_case ** 2                #nombre de couleurs nécessaires (4 ou 9) 
        self.cote_case = cote_case                      #taille du sous bloc carré (2 ou 3)
        self.adj = self.generer_graphe()

    def liste_voisins_ligne(self, case):
        """Retourne les numéros des cases sur la même ligne de la grille"""
        lig_case, col_case = case // self.nbcouleurs, case % self.nbcouleurs
        return [lig_case * self.nbcouleurs + k for k in range(self.nbcouleurs) if k != col_case]

    def liste_voisins_colonne(self, case):
        """Retourne les numéros des cases sur la même colonne de la grille"""
        lig_case, col_case = case // self.nbcouleurs, case % self.nbcouleurs
        return [col_case + self.nbcouleurs * k for k in range(self.nbcouleurs) if k != lig_case]

    def liste_voisins_bloc(self, case):
        """Retourne les numéros des cases dans le même sous bloc carré"""
        #on détermine la ligne et la colonne du bloc carré correspondant avec lig_case//self.cote_case et col_case//self.cote_case
        #dans chaque chaque ligne de blocs, il y a self.cote_case * self.nbcouleurs valeurs
        #dans chaque bloc, il y a self.cote_case colonnes        
        lig_case, col_case = case // self.nbcouleurs, case % self.nbcouleurs
        deb_case = (lig_case // self.cote_case) * self.cote_case * self.nbcouleurs + (col_case // self.cote_case) * self.cote_case
        bloc = []
        for c in range(self.cote_case) :
            for d in range(self.cote_case) :
                autre_case = deb_case + c  + d  * self.nbcouleurs 
                if autre_case != case :
                    bloc.append(autre_case)
        return bloc
    
    def generer_graphe(self) :
        """génère le graphe des contraintes sous la forme d'un tableau"""
        #self.adj[sommet] correspond à la listes des sommets qui ne doivant pas avoir la même couleur que lui
        adj = [[] for i in range(self.cote_case ** 4)]
        for case in range(self.nbcases):            
            for autre_case in self.liste_voisins_ligne(case):
                adj[autre_case].append(case)
            for autre_case in self.liste_voisins_colonne(case):
                adj[autre_case].append(case)
            for autre_case in self.liste_voisins_bloc(case):
                if case not in adj[autre_case]:
                    adj[autre_case].append(case)
        return adj

    def conversion_grille_vers_couleur(self, grille) :
        """permet de transformer un tableau de couleurs sous la forme d'une liste de couleurs (pour chaque sommet/case)"""
        assert len(grille) == self.nbcouleurs and all(len(grille[k]) == self.nbcouleurs for k in range(self.nbcouleurs)), 'grille de mauvaises dimensions'
        couleur = [0] * self.nbcases
        for lig in range(self.nbcouleurs) :
            for col in range(self.nbcouleurs) :
                case = lig * self.nbcouleurs + col
                couleur[case] = grille[lig][col]
        return couleur

    def conversion_couleur_vers_grille(self, couleur) :
        """permet de transformer une liste de couleurs sous la forme d'un tableau de couleurs"""
        grille = [[0] * self.nbcouleurs for k in range(self.nbcouleurs)]
        for k, c in enumerate(couleur) :
            grille[k // self.nbcouleurs][k % self.nbcouleurs] = c
        return grille
   
 
 
##################################################################
##          Résolution du sudoku par méthode gloutonne          ##
##################################################################

    @chronometre
    def resolution_sudoku_coloration_glouton(self, grille, itermax,  *args, **kwargs):
        """on parcourt la grille de la première à la dernière case en choisissant, pour les sommets non colorés,
        une couleur au hasard parmi les possibles"""
        for tentative in range(1, itermax + 1):
            couleur = self.conversion_grille_vers_couleur(grille)
            nombreAcolorier = couleur.count(0)
            case = 0
            while case < self.nbcases and nombreAcolorier > 0 :  
                if couleur[case] == 0 :
                    couleurs_possibles = list(range(1, self.nbcouleurs + 1))
                    for voisin in self.adj[case] :
                        if couleur[voisin] in couleurs_possibles :
                            couleurs_possibles.remove(couleur[voisin])
                    if couleurs_possibles != [] :
                        couleur[case] = random.choice(couleurs_possibles)
                        nombreAcolorier -= 1
                case += 1
            if nombreAcolorier == 0 :
                return (tentative, self.conversion_couleur_vers_grille(couleur))        
        return (itermax, None)


    @chronometre
    def resolution_sudoku_coloration_glouton_tri(self, grille, itermax, version,  *args, **kwargs):
        '''Résolution de sudoku par  coloration de graphe 
        voir http://www.cs.kent.edu/~dragan/ST-Spring2016/SudokuGC.pdf '''
        #version 'moins' : on choisit au hasard parmi ceux qui ont le moins de voisins non coloriés (couleur[voisin] == 0)      
        #version 'plus' : on choisit au hasard parmi ceux qui ont le plus de voisins non coloriés (couleur[voisin] == 0)
        #version 'hasard' : pas de tri préalable

        def plus_petit(a, b):
            return a < b
        
        def plus_grand(a, b):
            return a > b

        def selection_extremum_voisins(liste_candidat, couleur, valeur_initiale = float('inf'), comparaison = plus_petit):
            """Fonction qui retourne une case choisie aléatoirement parmi les cases dans liste_candidat
            telles que le nombre de voisins non coloriés est minimal (valeur_initiale = float('inf'), comparaison = plus_petit)
            ou maximal (valeur_initiale = 0, comparaison = plus_grand)
            """
            nb_voisins_blancs_extremum = valeur_initiale
            case_record = []
            for case in liste_candidat:
                nb_voisins_blancs = 0
                for voisin in self.adj[case]:
                    if couleur[voisin] == 0:
                        nb_voisins_blancs += 1
                if comparaison(nb_voisins_blancs,  nb_voisins_blancs_extremum):
                    nb_voisins_blancs_extremum  =  nb_voisins_blancs
                    case_record = [case]
                elif nb_voisins_blancs == nb_voisins_blancs_extremum:
                    case_record.append(case)
            #choix aléatoire
            return [random.choice(case_record)]
                
        def resolution():
            """Fonction de résolution de Sudoku par heuristique gloutonne randomisée"""
            #on récupère la liste des couleurs de chaque sommet/case
            couleur = self.conversion_grille_vers_couleur(grille)
            #liste des cases/sommets  à traiter (leur couleur n'est pas encore fixée)
            caseAFaire  = [case for case in range(self.nbcases)]   
            #liste des couleurs possibles pour chaque case   
            liste_couleur_possible = [ list(range(1, self.nbcouleurs + 1))  for case in range(self.nbcases)]
            #on initialise les cases dont les couleurs sont déjà fixées
            for case, color  in enumerate(couleur):
                #on fixe la couleur des cases déjà coloriées
                if color > 0:
                    liste_couleur_possible[case] = [color]  
            #tant qu'il y a des cases à traiter 
            while len(caseAFaire) > 0: 
                #liste des candidats à compléter           
                liste_candidat = []
                nbcouleur_min = self.nbcouleurs + 1
                #on sélectionne d'abord les sommets non traités qui ont le moins de couleurs possibles
                for case in caseAFaire:
                    #nombre de couleurs possibles pour le sommet
                    nbcouleur = len(liste_couleur_possible[case])
                    #mise à jour du minimum nbcouleurmin
                    if  nbcouleur < nbcouleur_min:
                        liste_candidat = [case]
                        nbcouleur_min = nbcouleur
                    elif nbcouleur == nbcouleur_min:
                        liste_candidat.append(case)
                #si on a trouvé des sommets pour lesquels il ne reste plus qu'une seule couleur possible
                #ce sont ces sommets qu'on va colorier et on ne change pas liste_candidat
                #sinon on distingue trois cas               
                if nbcouleur_min > 1:                    
                    #premier cas : on choisit parmi les sommets  dans liste_candidat
                    #ceux qui ont le moins   de voisins non coloriés
                    if version == 'moins' :
                        liste_candidat = selection_extremum_voisins(liste_candidat, couleur, valeur_initiale = 0, comparaison = plus_grand)
                    #deuxième cas : on choisit parmi les sommets  dans liste_candidat
                    #ceux qui ont le plus  de voisins non coloriés
                    elif version == 'plus' :                      
                        liste_candidat = selection_extremum_voisins(liste_candidat, couleur, valeur_initiale = float('inf'), comparaison = plus_petit)
                    #troisième cas : on choisit au hasard parmi les sommets  dans liste_candidat
                    else :
                        liste_candidat = [random.choice(liste_candidat)]                            
                #il reste à fixer la couleur des sommets dans liste_candidat
                #et à propager la contrainte sur leurs voisins                                       
                for case in liste_candidat:                    
                    if len(liste_couleur_possible[case]) == 0 : 
                        #plus de couleurs disponibles pour la case à colorier
                        #dans ce cas le coloriage ne fonctionne pas 
                        return  None
                    couleur[case] =   liste_couleur_possible[case].pop()
                    for voisin in self.adj[case]:
                        if couleur[case] in liste_couleur_possible[voisin]:
                            liste_couleur_possible[voisin].remove(couleur[case])
                    caseAFaire.remove(case)  
            #fini grille résolue
            return self.conversion_couleur_vers_grille(couleur)      

        #comme l'algorithme de coloriage est randomisé
        # on tente des essais successifs tant qu'on n'a pas résolu la grille
        # avec un nombre maximal d'itérations
        for k in range(1, itermax):
            solution= resolution()
            if solution is not None:
                return (k, solution)
        return  (itermax, None)
  

##################################################################
##         Résolution du sudoku par méthode backtracking        ##
##################################################################
    
    @chronometre
    def resolution_sudoku_backtrack(self, grille, *args, **kwargs) :
        """Dans cette  méthode on utilise le graphe des incompatibilités pour tester les couleurs 
        possibles pour une case mais on fait une exploration exhaustive sur un autre graphe plus petit
        (arbre peigne de racine la case 0):
        on parcourt un tableau 2D , la grille, en progressant de la case numéro 0 jusqu'à la dernière
        et en reculant et effaçant ses traces si on est bloqué (backtracking)"""       
        couleur = self.conversion_grille_vers_couleur(grille)
  
        def exploration(case, couleur):  
            if case == self.nbcases:
                solution = self.conversion_couleur_vers_grille(couleur)
                return (True, solution)
            if couleur[case] > 0:
                return exploration(case + 1, couleur)
            else:                
                couleurs_voisins = set()
                for voisin in self.adj[case] :
                    couleurs_voisins.add(couleur[voisin])
                for c in range(1, self.nbcouleurs + 1) :
                    if c not in couleurs_voisins:
                        couleur[case] = c
                        copie_couleur = couleur[:]
                        rep = exploration(case + 1, copie_couleur)
                        if rep[0]:
                            return rep
                #backtracking
                couleur[case] = 0
                return (False, None)
                
        #appel de la fonction d'exploration
        _, solution =  exploration(0, couleur)
        return solution

    @chronometre
    def resolution_sudoku_backtrack_tri(self, grille, *args, **kwargs) :
        '''Voir https://projecteuler.net/problem=96
           Voir http://igm.univ-mlv.fr/~dr/XPOSE2013/sudoku/backtracking.html#principe'''
        def couleurs_possibles(case, couleur):                
            crible = [True for _ in range(self.nbcouleurs)]
            couleurs_voisins = set()
            for voisin in self.adj[case] :
                couleurs_voisins.add(couleur[voisin])
            #print("C", couleurs_voisins)
            for c in couleurs_voisins :
                if c != 0: #attention aux index négatifs en python !
                    crible[c - 1] = False
            return [k + 1 for k, possible in enumerate(crible) if possible]

        couleur = self.conversion_grille_vers_couleur(grille)
        liste_cases_tri = sorted([case for case in range(self.nbcases) if couleur[case] == 0],
                                     key = lambda case : len(couleurs_possibles(case, couleur)))
        nb_cases_vides = len(liste_cases_tri)
        #liste_solutions = []

        def exploration(index_tri_case, couleur):  
            if index_tri_case == nb_cases_vides :
                return    (True, self.conversion_couleur_vers_grille(couleur))             
            case = liste_cases_tri[index_tri_case]      
            couleurs_voisins = set()
            for voisin in self.adj[case] :
                couleurs_voisins.add(couleur[voisin])        
            for c in range(1, self.nbcouleurs + 1) :
                if c not in couleurs_voisins:
                    couleur[case] = c
                    copie_couleur = couleur[:]
                    rep = exploration(index_tri_case  + 1, copie_couleur)
                    if rep[0]:
                        return rep
            #backtracking
            couleur[case] = 0  
            return (False, None)  
                    
        #appel de la fonction d'exploration
        _, solution =  exploration(0, couleur)
        return solution

   
    
