Pour générer un exécutable du script Python `sudoku_versionTKinterDemoBacktracking.py`  en suivant ce tutoriel <https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/235020-distribuer-facilement-nos-programmes-python-avec-cx-freeze>.


Ce dossier  contient les scripts Python nécessaires pour générer un exécutable sur n'importe quelle plateforme en exécutant `python3 setup.py build` sous Linux ou `py setup.py build` dans un environnement Anaconda sous Windows. Il faut d'abord installer le module  `cx-freeze` avec `pip`