#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable

# On appelle la fonction setup
setup(
    name = "backtrack-demo-tkinter",
    version = "1.0",
    description = "Interface graphique de demo de résolution de sudoku par backtracking",
    executables = [Executable("sudoku_versionTKinterDemoBacktracking.py")],
)
