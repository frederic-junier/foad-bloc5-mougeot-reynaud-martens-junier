#!/usr/bin/env python
# -*- coding: utf-8 -*-

##################################################################
##                Importation des bibliothèques                 ##
##################################################################

from sudoku_version_demo import Sudoku
from tkinter import *
from tkinter.messagebox import *

##################################################################
##                    Fonctions nécessaires                     ##
##################################################################

def affichage(coul):
    #0 pas de couleur, et ensuite on utilise les couleurs 1,2,3 et 4, ou plus si 9*9, selon la palette
    global can
    for k in range(len(coul)) :
        can.create_rectangle(k%nbCouleur*T+b,(k//nbCouleur)*T+b,(k%nbCouleur+1)*T+b,(k//nbCouleur+1)*T+b,fill=palette[coul[k]])
    for k in range(N-1) :
        can.create_line(b,b+T*N*(k+1),b+nbCouleur*T,b+T*N*(k+1), fill='black',width = 3)
        can.create_line(b+T*N*(k+1),b,b+T*N*(k+1),b+nbCouleur*T, fill='black',width = 3)

def initialisation(GRILLE) :
    global couleur, couleurInit, can, nbCouleur, T, H, b, solutionGrille 
    nbCouleur = N**2
    T = 40
    b = 20
    H = 2*b + T*nbCouleur    #j'ai choisi des cases de taille 40 avec 2 bords de 20...
    
    sudo = Sudoku(N)
    sudo.generer_graphe()
    couleurInit = sudo.conversion_grille_vers_couleur(GRILLE)   
    solutionGrille = sudo.resolution_sudoku_backtrack(GRILLE)[0]
    solutionGrille = sudo.conversion_grille_vers_couleur(solutionGrille)

    couleur = [k for k in couleurInit]           #contiendra la liste des couleurs trouvées par l'utilisateur, à défaut celles données

    zone.destroy()
    can = Canvas(fen, height = H+40, width = H, bg = 'beige') 
    can.grid(row = 0, columnspan = 3, padx=10, pady=10)
    bouton_sudoku = Button(fen,text="Sudoku",command=sudoku)
    bouton_sudoku.grid(row = 1, column = 0)
    bouton_verifier = Button(fen,text="Verifier",command=verifier)
    bouton_verifier.grid(row = 1, column = 1)
    bouton_solution = Button(fen,text="Solution",command=solution)
    bouton_solution.grid(row = 1, column = 2)
    bouton_quitter = Button(fen,text="Quitter",command=fen.destroy)
    bouton_quitter.grid(row = 3, columnspan = 3)
    can.create_rectangle(b,b,b+nbCouleur*T,b+nbCouleur*T,fill='white', width = 3)
    for k in range(N-1) :
        can.create_line(b,b+T*N*(k+1),b+nbCouleur*T,b+T*N*(k+1), fill='black',width = 3)
        can.create_line(b+T*N*(k+1),b,b+T*N*(k+1),b+nbCouleur*T, fill='black',width = 3)
    for j in range(N) :
        for k in range(N-1):
            can.create_line(b,b+T*(k+1+j*N),b+nbCouleur*T,b+T*(k+1+j*N), fill='black')
            can.create_line(b+T*(k+1+j*N),b,b+T*(k+1+j*N),b+nbCouleur*T, fill='black')

def sudoku() :
    global couleur                               #on réinitialise à chaque fois...
    couleur = [k for k in couleurInit]           #contiendra la liste des couleurs trouvées par l'utilisateur, à défaut celles données
    affichage(couleurInit)
    can.bind("<Button-1>",pointeur)
    
def verifier() :
     print(couleur)
     print(solutionGrille)
     if solutionGrille == couleur :
         showinfo('grille correcte','BRAVO !')
     else :
         showwarning('grille incorrecte','Je vous suggère de recommencer...')  

def solution():
    affichage(solutionGrille)

def changeCouleur(n):
   global couleur
   if couleurInit[n] == 0 :
       couleur[n] = (couleur[n] + 1) % (nbCouleur+1)
       return affichage(couleur)
   
def pointeur(event):
    global x,y
    x = event.x - b
    y = event.y - b
    #print(x,y)
    numero = x//T + y//T * nbCouleur
    #print(numero)
    changeCouleur(numero)

def pasApas():
    global L, compt, can
    changement = [{0:1},{1:8},{3:4},{4:2},{5:5},{},{5:6},{6:5},{},{4:5,5:0,6:0},{5:2},{},{5:6},{6:2},{},\
                  {4:6,5:0,6:0},{5:2},{6:5},{},{5:5,6:0},{6:2},{},{4:9,5:0,6:0},{5:2},{6:5},{},{5:5,6:0},{6:2},{},{5:6,6:0},{6:2},{8:5}]
    if compt < len(changement) :
        if changement[compt] == {} :
            showinfo("backtracking","Pas de couleur possible... \n Il faut revenir en arrière...")
        else :
            for sommet in changement[compt] :
                L[sommet] = changement[compt][sommet]
            affichage(L)
        compt += 1
    else :
        showinfo("veuillez quitter","La démonstration backtracking pas à pas s'arrête ici.")

def backtrack() :    
    global L, compt, can, nbCouleur, T, H, b, N
    L = [0,0,3,0,0,0,0,7,0,
         5,9,0,7,0,8,4,6,0,
         0,6,0,0,0,0,0,0,9,
         0,3,0,0,4,0,0,8,0,
         0,0,0,2,0,9,0,0,0,
         0,7,0,0,3,0,0,1,0,
         8,0,0,0,0,0,0,9,0,
         0,4,7,6,0,1,0,3,8,
         0,2,0,0,0,0,7,0,0]
    nbCouleur = 9
    N = 3
    T = 40
    b = 20
    H = 2*b + T*nbCouleur    #j'ai choisi des cases de taille 40 avec 2 bords de 20...   
    compt = 0
    zone.destroy()
    can = Canvas(fen, height = H+40, width = H, bg = 'beige') 
    can.grid(row = 0, columnspan = 2, padx=10, pady=10)
    bouton_pasApas = Button(fen,text="pas à pas",command=pasApas)
    bouton_pasApas.grid(row = 1, column = 0)
    bouton_quitter = Button(fen,text="Quitter",command=fen.destroy)
    bouton_quitter.grid(row = 1, column = 1)
    can.create_rectangle(b,b,b+nbCouleur*T,b+nbCouleur*T,fill='white', width = 3)
    for k in range(N-1) :
        can.create_line(b,b+T*N*(k+1),b+nbCouleur*T,b+T*N*(k+1), fill='black',width = 3)
        can.create_line(b+T*N*(k+1),b,b+T*N*(k+1),b+nbCouleur*T, fill='black',width = 3)
    for j in range(N) :
        for k in range(N-1):
            can.create_line(b,b+T*(k+1+j*N),b+nbCouleur*T,b+T*(k+1+j*N), fill='black')
            can.create_line(b+T*(k+1+j*N),b,b+T*(k+1+j*N),b+nbCouleur*T, fill='black')
    affichage(L)

    
    
    

##################################################################
##               Initialisation des variables                   ##
##################################################################

palette = ['white', 'blue', 'green','red', 'yellow', 'pink', 'cyan', 'orange', 'lime', 'purple']
couleur = []
couleurInit = []
    
def sudoku2():   
    global N 
    GRILLE2 = [[3,4,1,0],
               [0,2,0,0],
               [0,0,2,0],
               [0,1,4,3]]
    N = 2     
    initialisation(GRILLE2)


def sudoku3():
    global N    
    GRILLE3 = [[0,0,3,0,0,0,0,7,0],
               [5,9,0,7,0,8,4,6,0],
               [0,6,0,0,0,0,0,0,9],
               [0,3,0,0,4,0,0,8,0],
               [0,0,0,2,0,9,0,0,0],
               [0,7,0,0,3,0,0,1,0],
               [8,0,0,0,0,0,0,9,0],
               [0,4,7,6,0,1,0,3,8],
               [0,2,0,0,0,0,7,0,0]]
    N = 3
    initialisation(GRILLE3)


##################################################################
##                      Partie graphique                        ##
##################################################################
#Création de la fenêtre de jeu
fen = Tk()

#Inscription du titre de la fenêtre
fen.title('Sudoku')

zone = Canvas(fen, width = 500, height = 400)
zone.grid(row = 0)
#Création de deux widget Label pour expliquer le choix
titre = Label(zone, text = 'SUDOKU')
titre.grid(row=0, columnspan = 3, pady=10)
texte = Label(zone, text = 'Que voulez-vous faire?')
texte.grid(row=1, columnspan = 3, padx=10, pady=20)
#Création de deux widget Button pour choisir
bouton_sudoku2 = Button(zone,text="Résoudre un Sudoku 4*4",command=sudoku2)
bouton_sudoku2.grid(row = 2, column = 0, padx=10, pady=10)
bouton_sudoku3 = Button(zone,text="Résoudre un Sudoku 9*9",command=sudoku3)
bouton_sudoku3.grid(row = 2, column = 1, padx=10, pady=10)
bouton_sudoku4 = Button(zone,text="Bactracking pas à pas",command=backtrack)
bouton_sudoku4.grid(row = 2, column = 2, padx=10, pady=10)

#Boucle infinie pour que la fenêtre reste ouverte
fen.mainloop()

