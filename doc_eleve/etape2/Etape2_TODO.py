## Des fonctions auxilliaires
# Exercice 2.1
def liste_voisins_ligne(n, case):
    """Retourne les numéros des cases sur la même ligne de la grille"""
    nbcouleurs = n**2
    #TODO
    return []

# print(liste_voisins_ligne(2,0)==[1,2,3])
# print(liste_voisins_ligne(3,9)==[10, 11, 12, 13, 14, 15, 16, 17])

# Exercice 2.1
def liste_voisins_colonne(n, case):
    """Retourne les numéros des cases sur la même colonne de la grille"""
    nbcouleurs = n**2
    #TODO
    return []

# print(liste_voisins_colonne(2,0)==[4, 8, 12])
# print(liste_voisins_colonne(3,9)==[0, 18, 27, 36, 45, 54, 63, 72])

# Exercice 2.3
def liste_voisins_bloc(n, case):
    """Retourne les numéros des cases dans le même sous bloc carré"""
    #on détermine la ligne et la colonne du bloc carré correspondant avec lig_case//n et col_case//n
    #dans chaque chaque ligne de blocs, il y a n * nbcouleurs valeurs
    #dans chaque bloc, il y a n colonnes
    nbcouleurs = n**2
    lig_case, col_case = case // nbcouleurs, case % nbcouleurs
    deb_case = (lig_case // n) * n * nbcouleurs + (col_case // n) * n
    bloc = []
    #TODO
    return bloc

# print(liste_voisins_bloc(2,0)==[4, 1, 5])
# print(liste_voisins_bloc(3,9)==[0, 18, 1, 10, 19, 2, 11, 20])

## Génération du graphe des contraintes
# Exercice 3
def generer_graphe(n) :
    """génère le graphe des contraintes sous la forme d'un tableau"""
    #adj[sommet] correspond à la listes des sommets qui ne doivant pas avoir la même couleur que lui
    nbcases = n**4
    adj = [[] for i in range(nbcases)]
    for case in range(nbcases):
        #TODO
        None
    return adj

# print(generer_graphe(2)==[[1, 2, 3, 4, 5, 8, 12], [0, 2, 3, 4, 5, 9, 13], [0, 1, 3, 6, 7, 10, 14], [0, 1, 2, 6, 7, 11, 15], [0, 1, 5, 6, 7, 8, 12], [0, 1, 4, 6, 7, 9, 13], [2, 3, 4, 5, 7, 10, 14], [2, 3, 4, 5, 6, 11, 15], [0, 4, 9, 10, 11, 12, 13], [1, 5, 8, 10, 11, 12, 13], [2, 6, 8, 9, 11, 14, 15], [3, 7, 8, 9, 10, 14, 15], [0, 4, 8, 9, 13, 14, 15], [1, 5, 8, 9, 12, 14, 15], [2, 6, 10, 11, 12, 13, 15], [3, 7, 10, 11, 12, 13, 14]])

## D'autres fonctions auxiliaires
# Exercice 4.1
def conversion_grille_vers_couleur(n, grille) :
    """permet de transformer un tableau 2D de couleurs sous la forme d'une liste 1D de couleurs (pour chaque sommet/case)"""
    nbcouleurs = n**2
    nbcases = n**4
    couleur = [0] * nbcases
    #TODO
    return couleur

# print(conversion_grille_vers_couleur(2,[[3,4,1,0],[0,2,0,0],[0,0,2,0],[0,1,4,3]])==[3, 4, 1, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 1, 4, 3])

#Exercice 4.2
def conversion_couleur_vers_grille(n, couleur) :
    """permet de transformer une liste de couleurs sous la forme d'un tableau de couleurs"""
    nbcouleurs = n**2
    grille = []
    #TODO
    return grille

# print(conversion_couleur_vers_grille(2,[3, 4, 1, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 1, 4, 3])==[[3, 4, 1, 0], [0, 2, 0, 0], [0, 0, 2, 0], [0, 1, 4, 3]])

## Vérification d'une grille de sudoku
# Exercice 5
import numpy as np
def verification_grille(n, grille):
    """Vérifie si une grille de Sudoku est correcte"""
    nbcouleurs = n**2
    reference = set(range(1, nbcouleurs + 1))
    #conversion de la grille en array numpy
    #les slices des array numpy permettent d'extraire les blocs plus facilement
    tableau = np.array(grille)
    for ligne in tableau:
        if set(ligne) != reference:
            return False
    for col in range(nbcouleurs):
        if set(tableau[:,col]) != reference:
            return False
    for lig_bloc in range(n):
        for col_bloc in range(n):
            lig_coin = lig_bloc * n
            col_coin = col_bloc * n
            if set(tableau[lig_coin:lig_coin + n,col_coin:col_coin + n].flatten()) != reference:
                return False
    return True

# print(verification_grille(2,[[1,2,3,4],[3,4,1,2],[2,1,4,3],[4,3,2,1]]))
# print(verification_grille(2,[[2,1,3,4],[3,4,1,2],[2,1,4,3],[4,3,2,1]]))
